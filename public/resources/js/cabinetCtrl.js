(function(){
	"use strict";
	let app = angular
				.module("testWorkApp", [])
				.controller('cabinetCtrl', function($scope, $location, $http) {
					$scope.showCodeContainer 		= false;
					$scope.showEditModuleContainer  = false;
					$scope.editModuleName			= "";
					$scope.editModuleCity			= "";
					$scope.editModuleDay			= "";
					$scope.editModuleOrientation	= "";
					$scope.modulesList				= [];
					$scope.editItemIndex;

					$http({
						method: 'get',
						url: '/user/getList'
					}).then(function (res) {
						$scope.modulesList = res.data;
					}).catch(function(res) {
						if (res) { console.log(res); }
					});

					$scope.onChangeCreateModuleName = function() {
						if ($scope.createModuleName) {
							$scope.showCodeContainer = true;
							$scope.createModuleCode = createCodeWidgetVerical(1, "Moscow");
						} else {
							$scope.showCodeContainer 	= false;
							$scope.createModuleCode 	= "";
						}
					}

					$scope.onChangeCreateModuleCity = function() {
						if ($scope.createModuleDay) {
							$scope.createModuleCode = createCodeWidgetVerical($scope.createModuleDay, $scope.createModuleCity);
						} else {
							$scope.createModuleCode = createCodeWidgetVerical(1, $scope.createModuleCity);
						}			
					}

					$scope.onChangeCreateModuleDay = function() {
						$scope.createModuleCode = createCodeWidgetVerical($scope.createModuleDay, $scope.createModuleCity);
					}

					$scope.onChangecreateModuleOrientation = function() {
						if ($scope.createModuleOrientation == "Vertical") {
							$scope.createModuleCode = createCodeWidgetVerical($scope.createModuleDay, $scope.createModuleCity);
						} else {
							$scope.createModuleCode = createCodeWidgetHorizontal($scope.createModuleDay, $scope.createModuleCity);
						}
					}

					$scope.createNew = function() {
						if (!$scope.createModuleName) { return alert('Bad Name'); }
						let object = {
								name: 			$scope.createModuleName.trim(),
								city: 			($scope.createModuleCity) ? $scope.createModuleCity.trim() : "Moscow",
								day: 			($scope.createModuleDay) ? $scope.createModuleDay.trim() : "1 day",
								orientation: 	($scope.createModuleOrientation) ? $scope.createModuleOrientation.trim() : "Vertical",
							}
						$http({
							method: 'post',
							url: '/user/create_module',
							data: {
								item: object
							}
						}).then(function (res) {
							$scope.modulesList.push(object);

							$scope.createModuleName 		= "";
							$scope.createModuleCity 		= "";
							$scope.createModuleDay 			= "";
							$scope.createModuleOrientation 	= "";
							$scope.createModuleCode 		= "";
							$scope.showCodeContainer		= false;
						}).catch(function(res) {
							if (res) { console.log(res.status); }
						});
					}

					$scope.editItem = function(item) {
						$scope.editItemIndex = $scope.modulesList.indexOf(item);
						$scope.showEditModuleContainer = true;
						$scope.editModuleName 			= $scope.modulesList[$scope.editItemIndex].name;
						$scope.editModuleCity 			= $scope.modulesList[$scope.editItemIndex].city;
						$scope.editModuleDay 			= $scope.modulesList[$scope.editItemIndex].day;
						$scope.editModuleOrientation 	= $scope.modulesList[$scope.editItemIndex].orientation;

						if ($scope.editModuleOrientation == 'Vertical') {
							$scope.editModuleCode = createCodeWidgetVerical($scope.editModuleDay, $scope.editModuleCity);
						} else {
							$scope.editModuleCode = createCodeWidgetHorizontal($scope.editModuleDay, $scope.editModuleCity);
						}	
					}
					
					$scope.onChangeEditModule = function() {
						if ($scope.editModuleOrientation == 'Vertical') {
							$scope.editModuleCode = createCodeWidgetVerical($scope.editModuleDay, $scope.editModuleCity);
						} else {
							$scope.editModuleCode = createCodeWidgetHorizontal($scope.editModuleDay, $scope.editModuleCity);
						}	
					}

					$scope.onChangeModuleName = function() { $scope.editModuleName = this.editModuleName; }

					$scope.saveEditItem = function() {
						if (!$scope.editModuleName) { return alert('Bad name!'); }

						let arraySend = [];
						$scope.modulesList[$scope.editItemIndex].name 			= $scope.editModuleName;
						$scope.modulesList[$scope.editItemIndex].city 			= $scope.editModuleCity;
						$scope.modulesList[$scope.editItemIndex].day			= $scope.editModuleDay;
						$scope.modulesList[$scope.editItemIndex].orientation 	= $scope.editModuleOrientation;

						for (let i = $scope.modulesList.length - 1; i >= 0; --i) {
							arraySend.push({
								city: 			$scope.modulesList[i].city,
								day: 			$scope.modulesList[i].day,
								name: 			$scope.modulesList[i].name,
								orientation: 	$scope.modulesList[i].orientation,
							})
						}

						$http({
							method: 'post',
							url: '/user/edit_list',
							data: {
								editArray: arraySend.reverse()
							}
						}).then(function (res) {
							$scope.editModuleName			= "";
							$scope.editModuleCity			= "";
							$scope.editModuleDay			= "";
							$scope.editModuleOrientation	= "";
							$scope.showEditModuleContainer 	= false;
							$scope.editItemIndex = -1;

						}).catch(function(res) {
							if (res) { console.log(res); }
						});

					}

					$scope.removeItem = function(item) {
						let arraySend = [];
						let index = $scope.modulesList.indexOf(item);
							$scope.modulesList.splice(index, 1);
						for (let i = $scope.modulesList.length - 1; i > 0; --i) {
							arraySend.push({
								city: 			$scope.modulesList[i].city,
								day: 			$scope.modulesList[i].day,
								name: 			$scope.modulesList[i].name,
								orientation: 	$scope.modulesList[i].orientation,
							})
						}

						$http({
							method: 'post',
							url: '/user/edit_list',
							data: {
								editArray: arraySend.reverse()
							}
						}).then(function (res) {
							$scope.showEditModuleContainer  = false;
							$scope.editModuleName			= "";
							$scope.editModuleCity			= "";
							$scope.editModuleDay			= "";
							$scope.editModuleOrientation	= "";
							$scope.editItem = {};
						}).catch(function(res) {
							if (res) { console.log(res); }
						});
					}
				});
})();

function createCodeWidgetVerical(int, cityPar) {
	var src = "http://localhost:3000/resources/html/vertical_1.html";
	var city = "294021";
	var width = 1350;
	var height = 200;

	switch(int) {
		case "1 day": 
			src = "http://localhost:3000/resources/html/vertical_1.html";
			break;
		case "3 days":
			src = "http://localhost:3000/resources/html/vertical_3.html";
			width = 1350;
			height = 500;
			break;
		case "5 days":
			src = "http://localhost:3000/resources/html/vertical_5.html";
			break;
	}
		switch(cityPar) {
			case "Moscow": 
				city = "294021";
				break;
			case "St. Petersburg":
				city = "295212";
				break;
			case "Nizhny Novgorod":
				city = "294199";
				break;
	}
	return "<!-- Виджет --><iframe id=\"weather-widget\" src=\"" + src + '#' + city +"\" frameborder=\"0\" scrolling=\"no\" width=\"" + width + "\" height=\"" + height + "\"></iframe><!-- / Виджет -->";
}

function createCodeWidgetHorizontal(int, cityPar) {
	var src = "http://localhost:3000/resources/html/horizontal_1.html";
	var city = "294021";
	var width = 1350;
	var height = 200;

	switch(int) {
		case "1 day":
			src = "http://localhost:3000/resources/html/horizontal_1.html";
			break;
		case "3 days":
			src = "http://localhost:3000/resources/html/horizontal_3.html";
			width = 1350;
			height = 500;
			break;
		case "5 days":
			src = "http://localhost:3000/resources/html/horizontal_5.html";
			width = 1350;
			height = 800;
			break;
		}

	switch(cityPar) {
		case "Moscow":
			city = "294021";
			break;
		case "St. Petersburg":
			city = "294021";
			break;
		case "Nizhny Novgorod":
			city = "294199";
			break;
	}
	return "<!-- Виджет --><iframe id=\"weather-widget\" src=\"" + src + '#' + city +"\" frameborder=\"0\" scrolling=\"no\" width=\"" + width + "\" height=\"" + height + "\"></iframe><!-- / Виджет -->";
}
