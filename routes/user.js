let router = require('express').Router();

let UserController 	= require('./user/index');
let userRouter 		= new UserController();

let passport = require('../auth.js');

let CheckAuthenticated = require('../middleware/checkAuthenticated.js');
let check = new CheckAuthenticated();

router.get('/signup', 				check.isUserNotAuthorisation, 	userRouter.signup);
router.get('/login', 				check.isUserNotAuthorisation, 	userRouter.login);
router.get('/cabinet', 				check.isUserAuthorisation, 		userRouter.cabinet);
router.get('/user/getList', 		check.isUserAuthorisation, 		userRouter.getListById);
router.post('/user/create_module', 	check.isUserAuthorisation, 		userRouter.createModule);
router.post('/user/edit_list', 		check.isUserAuthorisation, 		userRouter.editModule);

router.post('/signup', 	check.isUserNotAuthorisation, 	userRouter.saveUser);
router.post('/login', 	passport.authenticate('local'), userRouter.loginUser);

// TODO: in class userRouter
router.get('/test', function(req, res) {
	res.render('test');
})
router.get('/', function(req, res) {
	res.redirect('/login');
})

module.exports = router;