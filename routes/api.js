let router = require('express').Router();

let ApiController 	= require('./api/index');
let apiRouter 		= new ApiController();

router.get('/weather/:day/:city', 	apiRouter.getWidget);

module.exports = router;