let request = require('request');

class ApiController {
	constructor() {}

	/**
	 * get /weather/:day/:city
	 * @param  {object} req
	 * @param  {object} res
	 * @return {array of object} wether
	 */
	async getWidget(req, res) {
		let response;
		try {
			response = await new Promise(
				function(resolve, reject) {
					request('http://dataservice.accuweather.com/forecasts/v1/daily/5day/' + req.params.city + '?apikey=SGWxJH7QwD44JVRUGW8uBitIpTvPn5gG', function(error, response, body) {
						if (error && response.statusCode != 200) { reject(error) }
						let res = JSON.parse(body);
						let day = parseInt(req.params.day) > 6 ? 5 : parseInt(req.params.day);
						let city = "Москва";
						// TODO: hardcode. in constant
						switch (req.params.city) {
							case "294199":
								city = "Нижний Новгород";
								break;
							case "294021":
								city = "Москва";
								break;
							case "295212":
								city = " Санкт-Петербург";
								break;
						}
						let arraySend = [];
						for (let i = 0; i < day; ++i) {
							let date = new Date(res.DailyForecasts[i].Date);
							arraySend.push({
								date: 			date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate(),
								temperature: 	Math.round(((res.DailyForecasts[i].Temperature.Maximum.Value - 32) * 5) / 9),
								city: 			city
							})
						}
						resolve(arraySend);
					})
				}
			)
		} catch (err) {
			return res.status(500).json([]);
		}

		res.status(200).json(response);	
	}
}

module.exports = ApiController;