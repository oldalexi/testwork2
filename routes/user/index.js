let User = require('../../models/user_data/user').User;
// TODO: chk description function
class UserController {
	constructor() {}

	/**
	 * get login
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string} title
	 * @render {template} login
	 */
	login(req, res) {
		return res.status(200).render("login", { title: 'Log in' });
	}

	/**
	 * get signup
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string} title
	 * @render {template} signup
	 */
	signup(req, res) {
		return res.status(200).render("signup", { title: 'Sign up', message: '' });
	}

	/**
	 * post cabinet
	 * @param  {object} req
	 * @param  {object} res
	 * @return {object, object} session, user_data
	 * @render {template} cabinet
	 */

	async cabinet(req, res) {
		let user;

		try {
			user = await User.findOne({ _id: req.session.passport.user });
		} catch (err) {
			return res.status(500).render('error', { message: err.message, error: err });
		}

		if (user) {
			let date = new Date(user.created);
			date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
			return res.status(200).render('cabinet', {
				title: 				"cabinet",
				user_email: 		user.email,
				user_date: 			date,
				user_array_modules: user.arrayModules
			});
		}

		return res.status(200).redirect("/login");
	}

	/**
	 * post login
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string} title
	 * @render {} signup
	 */
	loginUser(req, res) {
		return res.status(200).redirect("/login");
	}

	/**
	 * post signup
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string, object} title, user_data
	 * @redirect {} login
	 */
	async saveUser(req, res) {
		let user;

		try {
			user = await User.findOne({ email: req.body.email });
		} catch (err) {
			return res.status(500).render('error', { message: err.message, error: err });
		}

		if (user) { 
			return res.status(401).render("signup", { title: 'Sign up', message: 'User already exists' }); 
		}

		let newUser = new User({
			email: req.body.email,
			password: req.body.password,
		});

		try {
			user = await newUser.save();
		} catch (err) {
			return res.status(500).render('error', { message: err.message, error: err });
		}

		return res.status(200).redirect("/login");
	}

	/**
	 * post signup
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string, object} title, user_data
	 * @redirect {} login
	 */
	async getListById(req, res) {
		let list;
		try {
			list = await User.findOne({ _id: req.session.passport.user });
		} catch (err) {
			return res.status(500).json(err);
		}

		res.status(200).json(list.arrayModules);
	}

	/**
	 * post signup
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string, object} title, user_data
	 * @redirect {} login
	 */
	async createModule(req, res) {
		let list;
		try {
			list = await User.findOne({ _id: req.session.passport.user });
		} catch (err) {
			return res.status(500).render('error', { message: err.message, error: err });
		}

		list.arrayModules.push(req.body.item);

		try {
			list = await (list.save());
		} catch (err) {
			return res.status(500).json(err);
		}
		return res.status(200).json(list.arrayModules);
	}

	/**
	 * post signup
	 * @param  {object} req
	 * @param  {object} res
	 * @return {string, object} title, user_data
	 * @redirect {} login
	 */
	async editModule(req, res) {
		let list;
		try {
			list = await User.findOne({ _id: req.session.passport.user });
		} catch (err) {
			return res.status(500).render('error', { message: err.message, error: err });
		}

		list.arrayModules = req.body.editArray;

		try {
			list = await (list.save());
		} catch (err) {
			return res.status(500).json(err);
		}

		return res.status(200).json({});
	}
}

module.exports = UserController;


