class CheckAuthenticated {

	isUserAuthorisation (req, res, next) {
		req.isAuthenticated() ? next() : res.redirect('/login');
	}

	isUserNotAuthorisation (req, res, next) {
		!req.isAuthenticated() ? next() : res.redirect('/cabinet');
	}
}

module.exports = CheckAuthenticated;