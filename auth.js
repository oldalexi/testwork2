let passport 		= require('passport');
let LocalStrategy 	= require('passport-local').Strategy;

let User = require('./models/user_data/user').User;

passport.use(new LocalStrategy(
	{ usernameField: 'email', passwordField: 'password'},
	function(username, password, done) {
		User.findOne({ email: username }, function (err, user) {
			if (err) { return done(err); }
			if (!user) { return done(null, false, { message: 'Incorrect username.' }); }
			if (!user.checkPassword(password)) { return done(null, false, { message: 'Incorrect password.' }); }
	  		return done(null, user);
		});
	}
));

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		err ? done(err) : done(null, user);
	});
});

module.exports = passport;