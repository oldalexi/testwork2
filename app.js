let express 		= require('express');
let path 			= require('path');
let logger 			= require('morgan');
let session 		= require('express-session');
let cookieParser 	= require('cookie-parser');
let MongoStore 		= require('connect-mongo')(session);
let bodyParser 		= require('body-parser');
/* object for routing */
let user 	= require('./routes/user');
let api 	= require('./routes/api');

let config 		= require('./config');
let mongoose 	= require("./libs/mongoose/config");
let passport	= require('./auth.js');

var app = express();

app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Logger
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//session
app.use(session({
	secret: 			config.get('session:secret'),
	key: 				config.get('session:key'),
	cookie: 			config.get('session:cookie'),
	store: 				new MongoStore({ mongooseConnection: mongoose.connection }),
	saveUninitialized: 	true,
	resave: 			true
}));

app.use(passport.initialize());
app.use(passport.session());

// Routing
app.use('/api', api);
app.use('/', user);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.render('error', {
		message: err.message,
		error: {}
	});
	// TODO: global errors
	res.status(err.status).json({});
});

module.exports = app;